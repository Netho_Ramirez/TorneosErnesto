﻿using System.Windows;
using Torneos.BIZ;
using Torneos.COMMON.Entidades;
using Torneos.COMMON.Interfaces;
using Torneos.DAL;

namespace Torneos.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para CambiosDeContraseña.xaml
    /// </summary>
    public partial class CambiosDeContraseña : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorUsuarios manejadorUsuario;
        accion accionn;
        public CambiosDeContraseña()
        {
            InitializeComponent();
            manejadorUsuario = new ManejadorUsuario(new RepositorioGenerico<Usuarios>());
            PonerBotonesUsuarioEnEdicion(false);
            Limpiar();
            ActualizarTabla();
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void ActualizarTabla()
        {
            dtgUsuarios.ItemsSource = null;
            dtgUsuarios.ItemsSource = manejadorUsuario.Listar;

        }

        private void HabilitarBotones(bool habilitados)
        {          
            btnEditarUsuario.IsEnabled = habilitados;
            btnGuardarUsuario.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;            
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbContraseña.Clear();
            txbContraseña.IsEnabled = habilitadas;
            txbNombreUsuario.Clear();
            txbNombreUsuario.IsEnabled = habilitadas;
        }

        private void Limpiar()
        {
            txbNombreUsuario.Clear();
            txbContraseña.Clear();            
        }

        private void PonerBotonesUsuarioEnEdicion(bool v)
        {
            btnCancelar.IsEnabled = v;
            btnEditarUsuario.IsEnabled = !v;            
            btnGuardarUsuario.IsEnabled = v;                       
        }     

        private void btnEditarUsuario_Click(object sender, RoutedEventArgs e)
        {            
            Usuarios emp = dtgUsuarios.SelectedItem as Usuarios;
            if (emp != null)
            {
                PonerBotonesUsuarioEnEdicion(true);
                HabilitarCajas(true);     
                txbNombreUsuario.Text = emp.NombreUsuario;
                txbContraseña.Text = emp.Contraceña;
                accionn = accion.Editar;
                
            }
            else
            {
                MessageBox.Show("Seleccione el usuario para modificarlo", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnGuardarUsuario_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombreUsuario.Text) || string.IsNullOrEmpty(txbContraseña.Text))
            {
                MessageBox.Show("Todos los campos son obligatorios", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }           
            if (accionn == accion.Nuevo)
            {
                Usuarios emp = new Usuarios()
                {                    
                    NombreUsuario = txbNombreUsuario.Text,
                    Contraceña = txbContraseña.Text,
                };
                if (manejadorUsuario.Agregar(emp))
                {
                    MessageBox.Show("Usuario registrado correctamente", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Information);
                    Limpiar();
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    PonerBotonesUsuarioEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Usuario no se hapodido agregar", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Usuarios emp = dtgUsuarios.SelectedItem as Usuarios;
                emp.NombreUsuario = txbNombreUsuario.Text;
                emp.Contraceña = txbContraseña.Text;                


                if (manejadorUsuario.Modificar(emp))
                {
                    MessageBox.Show("Usuario editado correctamente", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Information);
                    Limpiar();
                    ActualizarTabla();
                    PonerBotonesUsuarioEnEdicion(false);
                    HabilitarCajas(false);
                    HabilitarBotones(true);
                }
                else
                {
                    MessageBox.Show("El Usuario no se ha podido editar", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            Limpiar();
            PonerBotonesUsuarioEnEdicion(false);
            HabilitarBotones(true);
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txbContraseña_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {

        }
    }
}
