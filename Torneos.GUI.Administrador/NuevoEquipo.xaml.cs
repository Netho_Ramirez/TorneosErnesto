﻿using System.Windows;
using Torneos.BIZ;
using Torneos.COMMON.Entidades;
using Torneos.COMMON.Interfaces;
using Torneos.DAL;

namespace Torneos.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para NuevoEquipo.xaml
    /// </summary>
    public partial class NuevoEquipo : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        accion accionn;
        IManejadorEquipos manejadorEquipo;
        IManejadorDeportes manejadorDeporte;
        public NuevoEquipo()
        {
            InitializeComponent();
            manejadorEquipo = new ManejadorEquipos(new RepositorioGenerico<Equipos>());
            manejadorDeporte = new ManejadorDeDeportes(new RepositorioGenerico<Deportes>());
            PonerBotonesEquiposEnEdicion(false);
            Limpiar();
            ActualizarTabla();
            Combo();
            HabilitarBotones(true);
            HabilitarCajas(false);
        }

        private void ActualizarTabla()
        {
            dtgEquipos.ItemsSource = null;
            dtgEquipos.ItemsSource = manejadorEquipo.Listar;

        }

        private void HabilitarBotones(bool habilitados)
        {
            btnNuevoEquipo.IsEnabled = habilitados;
            btnEliminarEquipo.IsEnabled = habilitados;
            btnEditarEquipo.IsEnabled = habilitados;
            btnGuardarEquipo.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombreEquipo.Clear();
            txbNombreEquipo.IsEnabled = habilitadas;
            cmbDeporte.Text="";
            cmbDeporte.IsEnabled = habilitadas;
        }

        private void Limpiar()
        {
            txbNombreEquipo.Clear();
            cmbDeporte.ItemsSource = null;         
        }

        private void PonerBotonesEquiposEnEdicion(bool v)
        {
            btnCancelar.IsEnabled = v;
            btnEditarEquipo.IsEnabled = !v;
            btnEliminarEquipo.IsEnabled = !v;
            btnGuardarEquipo.IsEnabled = v;
            btnNuevoEquipo.IsEnabled = !v;
        }

        private void Combo()
        {
            cmbDeporte.ItemsSource = null;
            cmbDeporte.ItemsSource = manejadorDeporte.Listar;
        }


        private void btnNuevoEquipo_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            Limpiar();
            PonerBotonesEquiposEnEdicion(true);
            Combo();
            accionn = accion.Nuevo;
        }

        private void btnEditarEquipo_Click(object sender, RoutedEventArgs e)
        {
            Equipos emp = dtgEquipos.SelectedItem as Equipos;
            if (emp != null)
            {
                HabilitarCajas(true);
                cmbDeporte.Text = emp.Deporte;
                txbNombreEquipo.Text = emp.Nombre;                
                accionn = accion.Editar;
                PonerBotonesEquiposEnEdicion(true);

            }
        }

        private void btnGuardarEquipo_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombreEquipo.Text) || cmbDeporte.Text == "")
            {
                MessageBox.Show("Todos los campos son obligatorios", "Deportivo", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (accionn == accion.Nuevo)
            {
                Equipos emp = new Equipos()
                {                    
                    Nombre = txbNombreEquipo.Text,                    
                    Deporte = cmbDeporte.Text,
                };

                if (manejadorEquipo.Agregar(emp))
                {
                    MessageBox.Show("Equipo agregado correctamente", "Deportivo", MessageBoxButton.OK, MessageBoxImage.Information);
                    Limpiar();
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    PonerBotonesEquiposEnEdicion(false);
                    Combo();
                }
                else
                {
                    MessageBox.Show("El equipo no se ha podido agregar", "Deportivo", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Equipos emp = dtgEquipos.SelectedItem as Equipos;
                emp.Nombre = txbNombreEquipo.Text;               
                emp.Deporte = cmbDeporte.Text;


                if (manejadorEquipo.Modificar(emp))
                {
                    MessageBox.Show("Equipo modificado correctamente", "Deportivo", MessageBoxButton.OK, MessageBoxImage.Information);
                    Limpiar();
                    ActualizarTabla();
                    PonerBotonesEquiposEnEdicion(false);
                    HabilitarCajas(false);
                    HabilitarBotones(true);
                }
                else
                {
                    MessageBox.Show("El Equipo no se pudo modificar", "Deportivo", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEliminarEquipo_Click(object sender, RoutedEventArgs e)
        {
            Equipos emp = dtgEquipos.SelectedItem as Equipos;
            if (emp != null)
            {
                if (MessageBox.Show("Confirme la eliminacion", "Deportivo", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEquipo.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Equipo  eliminado con exito", "Deportivo", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTabla();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el equipo", "Deportivo", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            Limpiar();
            PonerBotonesEquiposEnEdicion(false);
            HabilitarBotones(true);
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
