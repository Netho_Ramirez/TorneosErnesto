﻿using System.Windows;

namespace Torneos.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para Inicio.xaml
    /// </summary>
    public partial class Inicio : Window
    {
        public Inicio()
        {
            InitializeComponent();
        }        

        private void btnNuevoDeporte_Click(object sender, RoutedEventArgs e)
        {
            NuevoDeporte nuevoDeporte = new NuevoDeporte();
            nuevoDeporte.Show();
        }

        private void btnNuevoEquipo_Click(object sender, RoutedEventArgs e)
        {
            NuevoEquipo nuevoEquipo = new NuevoEquipo();
            nuevoEquipo.Show();
        }

        private void btnGenerarTorneo_Click(object sender, RoutedEventArgs e)
        {
            GenerarTorneo generarTorneo = new GenerarTorneo();
            generarTorneo.Show();
        }

        private void btnCambiarContraseña_Click(object sender, RoutedEventArgs e)
        {
            CambiosDeContraseña cambiosDeContraseña = new CambiosDeContraseña();
            cambiosDeContraseña.Show();
        }        

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
