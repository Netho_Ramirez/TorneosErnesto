﻿#pragma checksum "..\..\CambiosDeContraseña.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "17F76A19BE8B5F9A3B93A3488527CD2D091D0378"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Torneos.GUI.Administrador;


namespace Torneos.GUI.Administrador {
    
    
    /// <summary>
    /// CambiosDeContraseña
    /// </summary>
    public partial class CambiosDeContraseña : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 32 "..\..\CambiosDeContraseña.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txbNombreUsuario;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\CambiosDeContraseña.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEditarUsuario;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\CambiosDeContraseña.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardarUsuario;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\CambiosDeContraseña.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\CambiosDeContraseña.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnRegresar;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\CambiosDeContraseña.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dtgUsuarios;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\CambiosDeContraseña.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txbContraseña;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Torneos.GUI.Administrador;component/cambiosdecontrase%c3%b1a.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CambiosDeContraseña.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txbNombreUsuario = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.btnEditarUsuario = ((System.Windows.Controls.Button)(target));
            
            #line 40 "..\..\CambiosDeContraseña.xaml"
            this.btnEditarUsuario.Click += new System.Windows.RoutedEventHandler(this.btnEditarUsuario_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnGuardarUsuario = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\CambiosDeContraseña.xaml"
            this.btnGuardarUsuario.Click += new System.Windows.RoutedEventHandler(this.btnGuardarUsuario_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\CambiosDeContraseña.xaml"
            this.btnCancelar.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnRegresar = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\CambiosDeContraseña.xaml"
            this.btnRegresar.Click += new System.Windows.RoutedEventHandler(this.btnRegresar_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.dtgUsuarios = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 7:
            this.txbContraseña = ((System.Windows.Controls.TextBox)(target));
            
            #line 72 "..\..\CambiosDeContraseña.xaml"
            this.txbContraseña.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txbContraseña_TextChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

