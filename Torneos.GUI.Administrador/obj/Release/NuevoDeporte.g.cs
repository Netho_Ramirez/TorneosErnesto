﻿#pragma checksum "..\..\NuevoDeporte.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C5E91DFDC8051F70551A5EEADA1D6876D6B6BCFC"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Torneos.GUI.Administrador;


namespace Torneos.GUI.Administrador {
    
    
    /// <summary>
    /// NuevoDeporte
    /// </summary>
    public partial class NuevoDeporte : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 32 "..\..\NuevoDeporte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txbNombreDeporte;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\NuevoDeporte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNuevoDeporte;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\NuevoDeporte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEditarDeporte;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\NuevoDeporte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardarDeporte;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\NuevoDeporte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnEliminarDeporte;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\NuevoDeporte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\NuevoDeporte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnRegresar;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\NuevoDeporte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dtgDeportes;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\NuevoDeporte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txbIdDeporte;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Torneos.GUI.Administrador;component/nuevodeporte.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\NuevoDeporte.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txbNombreDeporte = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.btnNuevoDeporte = ((System.Windows.Controls.Button)(target));
            
            #line 40 "..\..\NuevoDeporte.xaml"
            this.btnNuevoDeporte.Click += new System.Windows.RoutedEventHandler(this.btnNuevoDeporte_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnEditarDeporte = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\NuevoDeporte.xaml"
            this.btnEditarDeporte.Click += new System.Windows.RoutedEventHandler(this.btnEditarDeporte_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnGuardarDeporte = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\NuevoDeporte.xaml"
            this.btnGuardarDeporte.Click += new System.Windows.RoutedEventHandler(this.btnGuardarDeporte_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnEliminarDeporte = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\NuevoDeporte.xaml"
            this.btnEliminarDeporte.Click += new System.Windows.RoutedEventHandler(this.btnEliminarDeporte_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 60 "..\..\NuevoDeporte.xaml"
            this.btnCancelar.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnRegresar = ((System.Windows.Controls.Button)(target));
            
            #line 68 "..\..\NuevoDeporte.xaml"
            this.btnRegresar.Click += new System.Windows.RoutedEventHandler(this.btnRegresar_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.dtgDeportes = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 9:
            this.txbIdDeporte = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

