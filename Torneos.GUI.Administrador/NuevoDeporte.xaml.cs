﻿using System;
using System.Windows;
using Torneos.BIZ;
using Torneos.COMMON.Entidades;
using Torneos.COMMON.Interfaces;
using Torneos.DAL;

namespace Torneos.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para NuevoDeporte.xaml
    /// </summary>
    public partial class NuevoDeporte : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        accion acciones;
        IManejadorDeportes manejadorDeporte;        
        public NuevoDeporte()
        {
            InitializeComponent();
            manejadorDeporte = new ManejadorDeDeportes(new RepositorioGenerico<Deportes>());
            PonerBotonesDeportesEnEdicion(false);
            Limpiar();
            HabilitarCajas(false);
            ActualizarTabla();
            HabilitarBotones(true);
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnNuevoDeporte.IsEnabled = habilitados;
            btnEliminarDeporte.IsEnabled = habilitados;
            btnEditarDeporte.IsEnabled = habilitados;
            btnGuardarDeporte.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void HabilitarCajas(bool habilitadas)
        {
            txbNombreDeporte.Clear();
            txbNombreDeporte.IsEnabled = habilitadas;            
        }

        private void ActualizarTabla()
        {
            dtgDeportes.ItemsSource = null;
            dtgDeportes.ItemsSource = manejadorDeporte.Listar;            
        }

        private void Limpiar()
        {
            txbNombreDeporte.Clear();            
        }

        private void PonerBotonesDeportesEnEdicion(bool v)
        {
            btnCancelar.IsEnabled = v;
            btnEditarDeporte.IsEnabled = !v;
            btnEliminarDeporte.IsEnabled = !v;
            btnGuardarDeporte.IsEnabled = v;
            btnNuevoDeporte.IsEnabled = !v;            
        }
        

        private void btnNuevoDeporte_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            Limpiar();
            PonerBotonesDeportesEnEdicion(true);
            acciones = accion.Nuevo;
        }

        private void btnEditarDeporte_Click(object sender, RoutedEventArgs e)
        {
            Deportes emp = dtgDeportes.SelectedItem as Deportes;
            if (emp != null)
            {
                HabilitarCajas(true);
                txbNombreDeporte.Text = emp.NombreDeporte;            
                acciones = accion.Editar;
                PonerBotonesDeportesEnEdicion(true);
            }
        }

        private void btnGuardarDeporte_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombreDeporte.Text))
            {
                MessageBox.Show("Falta el nombre del deporte", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (acciones == accion.Nuevo)
            {
                Deportes emp = new Deportes()
                {
                    NombreDeporte = txbNombreDeporte.Text,



                };
                if (manejadorDeporte.Agregar(emp))
                {
                    MessageBox.Show("El Deporte se agrego correctamente  ", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Information);
                    Limpiar();
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    PonerBotonesDeportesEnEdicion(false);                   
                }
                else
                {
                    MessageBox.Show("El deporte no se pudo registrar", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Deportes emp = dtgDeportes.SelectedItem as Deportes;
                emp.NombreDeporte = txbNombreDeporte.Text;



                if (manejadorDeporte.Modificar(emp))
                {
                    MessageBox.Show("El Deporte ha sido modificado correctamente  ", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Information);
                    Limpiar();
                    ActualizarTabla();
                    PonerBotonesDeportesEnEdicion(false);
                    HabilitarCajas(false);
                    HabilitarBotones(true);
                }
                else
                {
                    MessageBox.Show("No se ha podido actualizar el deorte", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEliminarDeporte_Click(object sender, RoutedEventArgs e)
        {
            Deportes emp = dtgDeportes.SelectedItem as Deportes;
            if (emp != null)
            {
                if (MessageBox.Show("Confirma la eliminacion", "Deportivos Netho", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorDeporte.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Deporte  eliminado con exito", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTabla();
                    }
                    else
                    {
                        MessageBox.Show("El deporte no pudo ser eliminado", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            Limpiar();
            PonerBotonesDeportesEnEdicion(false);
            HabilitarBotones(true);
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
