﻿using System;
using System.Collections.Generic;
using System.Windows;
using Torneos.BIZ;
using Torneos.COMMON.Entidades;
using Torneos.COMMON.Interfaces;
using Torneos.DAL;

namespace Torneos.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para GenerarTorneo.xaml
    /// </summary>
    public partial class GenerarTorneo : Window
    {
        enum accion
        {
            nuevo,
            editar
        }
        IManejadorEquipos manejadorEquipo;
        IManejadorDeportes manejadorDeportes;
        IManejadorTorneos manejadorTorneo;
        List<TipoDeDeporte> soloDeEste = new List<TipoDeDeporte>();
        List<Aleatorios> lista = new List<Aleatorios>();
        List<Aleatorios> lista2 = new List<Aleatorios>();
        List<Torneoss> torneoo = new List<Torneoss>();
        List<TipoDeDeporte> nuevaLista = new List<TipoDeDeporte>();
        string equipo1;
        string equipo2;
        public GenerarTorneo()
        {
            InitializeComponent();
            manejadorEquipo = new ManejadorEquipos(new RepositorioGenerico<Equipos>());
            manejadorTorneo = new ManejadorTorneos(new RepositorioGenerico<Torneoss>());
            manejadorDeportes = new ManejadorDeDeportes(new RepositorioGenerico<Deportes>());
            CargarTablaCombo();
            BotonesEnEdicion(false);
        }

        private void CargarTablaCombo()
        {
            cmbDeporteParaTorneo.ItemsSource = null;
            cmbDeporteParaTorneo.ItemsSource = manejadorDeportes.Listar;
            dtgPrueba.ItemsSource = null;
            dtgPrueba.ItemsSource = manejadorTorneo.Listar;
        }

        private void BotonesEnEdicion(bool a)
        {
            cmbDeporteParaTorneo.ItemsSource = "";
            clFechaTorneo.Text = "";
            cmbDeporteParaTorneo.IsEnabled = a;
            clFechaTorneo.IsEnabled = a;
            CargarTablaCombo();
            btnGenerarTorneo.IsEnabled = a;
            btnCancelarTorneo.IsEnabled = a;
            btnEliminarTorneo.IsEnabled = !a;
            btnNuevoTorneo.IsEnabled = !a;
        }  

        private void Limpiar(bool a)
        {
            cmbDeporteParaTorneo.ItemsSource = "";
            clFechaTorneo.Text = "";
            cmbDeporteParaTorneo.IsEnabled = a;
            clFechaTorneo.IsEnabled = a;
            btnGenerarTorneo.IsEnabled = a;
            btnCancelarTorneo.IsEnabled = a;
            btnEliminarTorneo.IsEnabled = !a;
            btnNuevoTorneo.IsEnabled = !a;
            btnGenerarTorneo.IsEnabled = a;
            btnCancelarTorneo.IsEnabled = a;
            btnNuevoTorneo.IsEnabled = !a;
        }

        public void PrimerEquipo()
        {
            int contador = 0;
            Aleatorios a = new Aleatorios();
            foreach (var item in lista)
            {
                contador++;
                if (contador == 1)
                {
                    equipo1 = item.Datos;
                    a.Datos = item.Datos;
                    lista.Remove(item);
                    lista2.Add(a);
                    break;
                }
            }

        }

        private void SegundoEquipo()
        {
            if (lista.Count >= 1)
            {
                Random r = new Random();
                int val = r.Next(1, lista.Count);
                int contador = 0;

                foreach (var item2 in lista)
                {
                    contador++;

                    if (contador == val)
                    {
                        equipo2 = item2.Datos;
                        lista.Remove(item2);
                        break;
                    }
                }
            }
        }

        private void PurosImpares(string palabra)
        {
            PrimerEquipo();
            SegundoEquipo();
            Torneoss a = new Torneoss()
            {
                Equipo1 = equipo1,
                Equipo2 = equipo2,
                Marcador_1 = 0,
                Marcador_2 = 0,
                Tipo_Deporte = palabra.ToUpper(),
                FechaProgramada = clFechaTorneo.Text,
            };
            torneoo.Add(a);
            manejadorTorneo.Agregar(a);
            dtgPrueba.ItemsSource = null;
            dtgPrueba.ItemsSource = manejadorTorneo.Listar;
        }

        private void CargarEquipos(string palabra)
        {
            foreach (var item in manejadorEquipo.Listar)
            {
                List<TipoDeDeporte> nueva = new List<TipoDeDeporte>();
                if (item.Deporte == palabra)
                {
                    TipoDeDeporte equipos = new TipoDeDeporte();
                    equipos.Nombre = item.Nombre;
                    equipos.TipoDeporte = palabra;
                    nueva.Add(equipos);
                }
            }
        }     

        private void btnNuevoTorneo_Click(object sender, RoutedEventArgs e)
        {
            BotonesEnEdicion(true);
            torneoo = new List<Torneoss>();
        }

        

        private void UltimoSobrantes(string palabra)
        {
            PrimerEquipo();
            if (lista.Count >= 1)
            {
                Random r = new Random();
                int val = r.Next(2, lista2.Count);
                int contador = 0;

                foreach (var item2 in lista2)
                {
                    contador++;

                    if (contador == val)
                    {
                        equipo2 = item2.Datos;
                        break;
                    }
                }
            }
            Torneoss a = new Torneoss()
            {
                Equipo1 = equipo1,
                Equipo2 = equipo2,
                Tipo_Deporte = palabra.ToUpper(),
                Marcador_1 = 0,
                Marcador_2 = 0,
                FechaProgramada = clFechaTorneo.Text,
            };
            torneoo.Add(a);
            manejadorTorneo.Agregar(a);
            dtgPrueba.ItemsSource = null;
            dtgPrueba.ItemsSource = manejadorTorneo.Listar;       
        }

        private void btnEliminarTorneo_Click(object sender, RoutedEventArgs e)
        {
            if (dtgPrueba.SelectedItem != null)
            {
                Torneoss a = dtgPrueba.SelectedItem as Torneoss;
                if (MessageBox.Show("Confirme la eliminacion de: " + a.Equipo1 + " VS " + a.Equipo2, "Deportivos Netho", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorTorneo.Eliminar(a.Id))
                    {
                        MessageBox.Show("El encuentro ha sido eliminado", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Information);
                        CargarTablaCombo();
                    }
                    else
                    {
                        MessageBox.Show("No se puedo eliminar el encuentro", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Porfavor seleccione elencuentro a eliminar", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnGenerarTorneo_Click(object sender, RoutedEventArgs e)
        {
            if (cmbDeporteParaTorneo.SelectedItem == null)
            {
                MessageBox.Show("Seleccione el deporte para generar el torneo", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string palabra = cmbDeporteParaTorneo.Text;
            if (manejadorEquipo.ContadorDeBuscarEquipo(palabra) <= 1)
            {
                MessageBox.Show("Para generar un torneo Debe de tener mas de un equipo (Agregue mas equipos al deporte de: " + cmbDeporteParaTorneo.Text, "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (string.IsNullOrEmpty(clFechaTorneo.Text))
            {
                MessageBox.Show("Falta llenar el campo de fecha del torneo", "Deportivos Netho", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            foreach (var item in manejadorEquipo.Listar)
            {
                if (item.Deporte == palabra)
                {
                    Aleatorios equipos = new Aleatorios();
                    equipos.Datos = item.Nombre;
                    lista.Add(equipos);
                }
            }

            if (lista.Count % 2 == 0)
            {
                while (((lista.Count) / 2) > 0)
                {
                    PurosImpares(palabra);
                }
            }
            else
            {
                while (((lista.Count) / 2) > 0)
                {
                    PurosImpares(palabra);
                }
                UltimoSobrantes(palabra);
            }

            Limpiar(false);
        }

        private void btnCancelarTorneo_Click(object sender, RoutedEventArgs e)
        {
            CargarTablaCombo();
            BotonesEnEdicion(false);            
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
