﻿using System.Windows;
using Torneos.BIZ;
using Torneos.COMMON.Entidades;
using Torneos.COMMON.Interfaces;
using Torneos.DAL;

namespace Torneos.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IManejadorUsuarios manejadorUsuario;

        public MainWindow()
        {
            InitializeComponent();
            manejadorUsuario = new ManejadorUsuario(new RepositorioGenerico<Usuarios>());
            cmbUsuario.ItemsSource = null;
            cmbUsuario.ItemsSource = manejadorUsuario.Listar;
        }

        private void btnEntrar_Click(object sender, RoutedEventArgs e)
        {
            if (cmbUsuario.Text == "")

            {
                MessageBox.Show("Porfavor seleccione un usuario", "Inicio", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            if (string.IsNullOrEmpty(txbContraseña.Password))
            {
                MessageBox.Show("Campo de contraseña vacio", "Inicio", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            if (cmbUsuario.SelectedItem != null)

            {

                Usuarios b = cmbUsuario.SelectedItem as Usuarios;
                if (txbContraseña.Password == b.Contraceña)
                {
                    Inicio ir = new Inicio();
                    ir.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Contraceña incorrecta", "Inicio", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

            }
            else
            {
                MessageBox.Show("Porfavor seleccione un usuario", "Inicio", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }
    }
}
