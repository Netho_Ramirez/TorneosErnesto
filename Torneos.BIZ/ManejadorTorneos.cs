﻿using System.Collections.Generic;
using System.Linq;
using Torneos.COMMON.Entidades;
using Torneos.COMMON.Interfaces;
using MongoDB.Bson;

namespace Torneos.BIZ
{
    public class ManejadorTorneos : IManejadorTorneos
    {
        IRepositorio<Torneoss> repositorio;
        public ManejadorTorneos(IRepositorio<Torneoss> repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Torneoss> Listar => repositorio.Read;

        public bool Agregar(Torneoss entidad)
        {
            return repositorio.Create(entidad);
        }

        public Torneoss BuscarPorId(ObjectId Id)
        {
            return Listar.Where(e => e.Id == Id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId Id)
        {
            return repositorio.Delete(Id);
        }

        public bool Modificar(Torneoss entidad)
        {
            return repositorio.Update(entidad);
        }

        public int VerificarSiEsNumero(string text)
        {
            foreach (var item in text)
            {
                if (!(char.IsNumber(item)))
                {
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
            return 1;
        }
    }
}
