﻿using System.Collections.Generic;
using System.Linq;
using Torneos.COMMON.Entidades;
using Torneos.COMMON.Interfaces;
using MongoDB.Bson;
using System.Collections;

namespace Torneos.BIZ
{
    public class ManejadorEquipos : IManejadorEquipos
    {
        IRepositorio<Equipos> repositorio;
        public ManejadorEquipos(IRepositorio<Equipos> repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Equipos> Listar => repositorio.Read;

        public bool Agregar(Equipos entidad)
        {
            return repositorio.Create(entidad);
        }

        public IEnumerable BuscarEquipos(string palabra)
        {
            return Listar.Where(e => e.Deporte == palabra).ToList();
        }

        public Equipos BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public int ContadorDeBuscarEquipo(string palabra)
        {
            return Listar.Where(e => e.Deporte == palabra).ToList().Count();
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorio.Delete(id);
        }

        public List<Equipos> EquiposPorNombre(string nombre)
        {
            return Listar.Where(e => e.Nombre == nombre).ToList();
        }

        public bool Modificar(Equipos entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}
