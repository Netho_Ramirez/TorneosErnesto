﻿using MongoDB.Bson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Torneos.COMMON.Entidades;
using Torneos.COMMON.Interfaces;

namespace Torneos.BIZ
{
    public class ManejadorDeDeportes : IManejadorDeportes
    {
        IRepositorio<Deportes> repositorio;
        public ManejadorDeDeportes(IRepositorio<Deportes> repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Deportes> Listar => repositorio.Read;

        public bool Agregar(Deportes entidad)
        {
            return repositorio.Create(entidad);
        }

        public Deportes BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorio.Delete(id);
        }

        public List<Deportes> DeportesPorNombre(string nombre)
        {
            return Listar.Where(e => e.NombreDeporte == nombre).ToList();
        }

        public bool Modificar(Deportes entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}
