﻿using System.Collections.Generic;
using System.Linq;
using Torneos.COMMON.Entidades;
using Torneos.COMMON.Interfaces;
using MongoDB.Bson;

namespace Torneos.BIZ
{
    public class ManejadorUsuario : IManejadorUsuarios
    {
        IRepositorio<Usuarios> repositorio;
        public ManejadorUsuario(IRepositorio<Usuarios> repositorio)
        {
            this.repositorio = repositorio;
        }

        public List<Usuarios> Listar => repositorio.Read;

        public bool Agregar(Usuarios entidad)
        {
            return repositorio.Create(entidad);
        }

        public Usuarios BuscarPorId(ObjectId id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(ObjectId id)
        {
            return repositorio.Delete(id);
        }

        public List<Usuarios> UsuarioPorNombre(string nombre)
        {
            return Listar.Where(e => e.NombreUsuario == nombre).ToList();
        }

        public bool Modificar(Usuarios entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}
