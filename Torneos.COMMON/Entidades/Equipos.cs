﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Torneos.COMMON.Entidades
{
    public class Equipos : Base 
    {
        public string Nombre { get; set; }        
        public string Deporte { get; set; }        
        public override string ToString()
        {
            return Nombre;
        }

    }
}
