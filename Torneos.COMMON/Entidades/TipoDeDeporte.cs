﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Torneos.COMMON.Entidades
{
    public class TipoDeDeporte
    {
        public string TipoDeporte { get; set; }
        public string Nombre { get; set; }
    }
}
