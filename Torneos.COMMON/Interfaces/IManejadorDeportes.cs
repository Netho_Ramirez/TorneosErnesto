﻿using System;
using System.Collections.Generic;
using System.Text;
using Torneos.COMMON.Entidades;

namespace Torneos.COMMON.Interfaces
{
    public interface IManejadorDeportes : IManejadorGenerico<Deportes>
    {
        List<Deportes> DeportesPorNombre(string nombre);
    }
}
