﻿using System;
using System.Collections.Generic;
using System.Text;
using Torneos.COMMON.Entidades;

namespace Torneos.COMMON.Interfaces
{
    public interface IManejadorUsuarios : IManejadorGenerico<Usuarios>
    {
        List<Usuarios> UsuarioPorNombre(string nombre);
    }
}
