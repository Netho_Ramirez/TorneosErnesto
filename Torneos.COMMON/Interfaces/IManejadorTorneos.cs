﻿using System;
using System.Collections.Generic;
using System.Text;
using Torneos.COMMON.Entidades;

namespace Torneos.COMMON.Interfaces
{
    public interface IManejadorTorneos : IManejadorGenerico<Torneoss>
    {        
        int VerificarSiEsNumero(string text);
    }
}
