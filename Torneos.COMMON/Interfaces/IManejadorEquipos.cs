﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Torneos.COMMON.Entidades;

namespace Torneos.COMMON.Interfaces
{
    public interface IManejadorEquipos : IManejadorGenerico<Equipos>
    {        
        List<Equipos> EquiposPorNombre(string nombre);
        int ContadorDeBuscarEquipo(string palabra);
        IEnumerable BuscarEquipos(string palabra);
    }
}
