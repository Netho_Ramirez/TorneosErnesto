﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using Torneos.COMMON.Entidades;

namespace Torneos.COMMON.Interfaces
{
    public interface IManejadorGenerico<T> where T : Base
    {
        bool Agregar(T entidad);
        List<T> Listar { get; }
        bool Eliminar(ObjectId id);
        bool Modificar(T entidad);
        T BuscarPorId(ObjectId id);
    }
}
